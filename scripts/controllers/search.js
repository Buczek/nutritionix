angular.module('nutriApp',['ngMaterial','nix.api'])
    .controller('SearchCtrl', ['$timeout', '$q', '$log', function ($timeout, $q, $log) {

        // ici j'arrive à récupérer la valeur du input mais ensuite ce qui devient difficile
        // c'est l'autocomplete de Angular material qu'il faut pouvoir relier au service
        // ça je ne vois pas comment faire

        // //var test;
        //
        // /*nixApi.autocomplete('apple').success(function (suggestions) {
        //     nutritionixservice.autocomplete = suggestions;
        // });*/
        //
        // this.simulateQuery = nutritionixservice.simulateQuery;
        // this.isDisabled    = nutritionixservice.isDisabled;
        //
        // // list of `state` value/display objects
        // this.states        = nutritionixservice.states;
        // this.querySearch   = nutritionixservice.querySearch;
        // this.selectedItemChange = nutritionixservice.selectedItemChange;
        // this.searchTextChange   = nutritionixservice.searchTextChange;
        //
        // this.newState = nutritionixservice.newState;



        var self = this;

        self.simulateQuery = false;
        self.isDisabled    = false;

        // list of `state` value/display objects
        self.states        = loadAll();
        self.querySearch   = querySearch;
        self.selectedItemChange = selectedItemChange;
        self.searchTextChange   = searchTextChange;

        self.newState = newState;

        function newState(state) {
            alert("Sorry! You'll need to create a Constitution for " + state + " first!");
        }

        // ******************************
        // Internal methods
        // ******************************


        // Search for states... use $timeout to simulate
        // remote dataservice call.

        function querySearch (query) {

            var results = query ? self.states.filter( createFilterFor(query) ) : self.states,
                deferred;
            if (self.simulateQuery) {
                deferred = $q.defer();
                $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
                return deferred.promise;
            } else {
                console.log(results);
                return results;
            }
        }

        function searchTextChange(text) {
            $log.info('Text changed to ' + text);
        }

        function selectedItemChange(item) {
            $log.info('Item changed to ' + JSON.stringify(item));
        }


        // Build `states` list of key/value pairs

        function loadAll() {
            var allStates = 'Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware,\
              Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Louisiana,\
              Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana,\
              Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina,\
              North Dakota, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, South Carolina,\
              South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, Washington, West Virginia,\
              Wisconsin, Wyoming';

            return allStates.split(/, +/g).map( function (state) {
                return {
                    value: state.toLowerCase(),
                    display: state
                };
            });
        }

        // Create filter function for a query string
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(state) {
                return (state.value.indexOf(lowercaseQuery) === 0);
            };

        }

    }]);


