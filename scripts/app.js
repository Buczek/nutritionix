// ajout des modules nécessaires à l'application
var nutriApp = angular.module("nutriApp", ['ngMaterial','ui.router','nix.api'] );


nutriApp.config(['$stateProvider', '$urlRouterProvider', function( $stateProvider, $urlRouterProvider ){

    $urlRouterProvider.otherwise('/');

    // route pour la recherche
    /*var homeState = {
        name: 'home',
        url: '/',
        controller: 'SearchCtrl as search'
    };
    $stateProvider.state(homeState);*/

  }]);